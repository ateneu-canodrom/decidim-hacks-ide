# frozen_string_literal: true

require "decidim_hacks/parser_helper"

module DecidimHacks
	module ProposalPresenterOverride
		extend ActiveSupport::Concern

		included do
			include DecidimHacks::ParserHelper
			alias original_body body

      def body(links: false, extras: true, strip_tags: false, all_locales: false)
				text = original_body(links: links, extras: extras, strip_tags: strip_tags, all_locales: all_locales)
				# Speciall render for course exercises (markdown processing)
				if participatory_space_slug.starts_with? "level"
					text = md_render(text)
					# hack to avoid the replacement of lines to <br> that simple_format does
					# return text.gsub(">\n",">").gsub("\n<","<")
				end
				text
			end

			def participatory_space_slug
				begin
					proposal.participatory_space.slug
				rescue
					""
				end
			end
		end
	end
end
