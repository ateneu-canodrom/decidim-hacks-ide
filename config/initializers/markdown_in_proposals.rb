# frozen_string_literal: true

require "decidim_hacks/proposal_presenter_override"

Rails.application.config.to_prepare do
  Decidim::Proposals::ProposalPresenter.include DecidimHacks::ProposalPresenterOverride
end